FROM python:3.8-slim

# https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
    && apt-get install --no-install-recommends -y curl=7.* gnupg2=2.* \
    && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install --no-install-recommends -y nodejs=12.* \
    && npm install -g serverless@2.* \
    && npm install --save serverless-google-cloudfunctions@3.* \
    && npm install --save serverless-python-requirements@5.* \
    && npm install --save serverless-wsgi@1.* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /
WORKDIR /
RUN pip install --no-cache-dir -r requirements.txt

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python3", "/main.py"]
