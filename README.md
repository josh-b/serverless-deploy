# Bitbucket Pipelines Pipe: Serverless deploy

Deploy a Serverless application to AWS and Google Cloud.
The pipe should also work with Azure, however support is currently limited and not guaranteed.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/serverless-deploy:1.1.1
    variables:
      # CONFIG: "<string>" # Optional
      # EXTRA_ARGS: "<string>" # Optional
      # PRE_EXECUTION_SCRIPT: '<string>' # Optional.
      # DEBUG: "<boolean>" # Optional
```

## Variables

### Base variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| CONFIG               | Path to the Serverless framework configuration file. Default: `serverless.yml`. |
| EXTRA_ARGS           | Extra options to pass to the `serverless deploy` command as a string. These options depend on the deployment provider. Default: `''`|
| PRE_EXECUTION_SCRIPT | Path to pre-execution script to execute additional specific actions needed. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |


### Provider related variables

Deployment credentials are cloud provider specific.

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID     | AWS key id. Default: `None`. |
| AWS_SECRET_ACCESS_KEY | AWS secret key. Default: `None`. |
| GCP_KEY_FILE          | base64 encoded contents of the GCP key file. Default: `None`. |
| AZURE_SUB_ID          | Azure subscription ID. Default: `None`. |
| SERVERLESS_ACCESS_KEY | Serverless access key for optional integration with the Serverless dashboard. Default: `None` |
| AZURE_SERVICE_PRINCIPAL_TENANT_ID | Azure service principal tenant ID. Default: `None`. |
| AZURE_SERVICE_PRINCIPAL_CLIENT_ID | Azure service principal client ID. Default: `None`. |
| AZURE_SERVICE_PRINCIPAL_PASSWORD | Azure service principal password. Default: `None`. |

_(*) = required variable._


## Prerequisites

* In order to use this pipe you need to create a Serverless application first.
* You will also need credentials for the cloud provider you will be deploying to.
* Check out the documentation for the cloud provider you are interested in the [official serverless documentation](https://serverless.com/framework/docs/).


*Google Cloud Platform:*

* Create a GCP project.
* Create an IAM member with at least a minimum set of roles: Cloud Functions Developer, Deployment Manager Editor, Storage Object Admin, and other needed for your resources.
* Create service account for your project.
* Create, download and save private_key.json. You will use it as your credentials in the `serverless.yml`.
* Enable the [Cloud Functions API][enable gcloud API].
* Enable the [Cloud Deployment Manager V2 API][enable gcloud API].


For base64 encoded contents required by GCP_KEY_FILE variable use command:

Linux
```
$ base64 -w 0 < GCP_key
```

MAC OS X
```
$ base64 < GCP_key
```


## Examples

Basic example deploying to AWS:

```yaml
script:
  - pipe: atlassian/serverless-deploy:1.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
```

Exmaple deploying to Google Cloud:

```yaml
script:
  - pipe: atlassian/serverless-deploy:1.1.1
    variables:
      GCP_KEY_FILE: $GCP_KEY_FILE
```

Example passing non-default config file `serverless.ts`:

```yaml
script:
  - pipe: atlassian/serverless-deploy:1.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      CONFIG: 'serverless.ts'
```


Example passing extra args:

```yaml
script:
  - pipe: atlassian/serverless-deploy:1.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      EXTRA_ARGS: '--verbose'
```

Example passing preexecution hook script file (file should have at least read and execute permissions):

```yaml
script:
  - echo 'npm install serverless-additional@2.*' > .my-script.sh
  - chmod 005 my-script.sh
  - pipe: atlassian/serverless-deploy:1.1.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      PRE_EXECUTION_SCRIPT: '.my-script.sh'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


[enable gcloud API]: https://cloud.google.com/endpoints/docs/openapi/enable-api
[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,serverless,lambda