import os
import sys
import subprocess
import base64

import yaml

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'CONFIG': {'type': 'string', 'required': False, 'nullable': True, 'default': 'serverless.yml'},
    'EXTRA_ARGS': {'type': 'string', 'required': False, 'nullable': True, 'default': ''},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
    'GCP_KEY_FILE': {'type': 'string', 'required': False, 'nullable': True, 'default': None},
    'SERVERLESS_ACCESS_KEY': {'type': 'string', 'required': False, 'nullable': True, 'default': None},
    'AWS_ACCESS_KEY_ID': {'type': 'string', 'required': False},
    'AWS_SECRET_ACCESS_KEY': {'type': 'string', 'required': False},
    'AZURE_SUB_ID': {'type': 'string', 'required': False},
    'AZURE_SERVICE_PRINCIPAL_TENANT_ID': {'type': 'string', 'required': False},
    'AZURE_SERVICE_PRINCIPAL_CLIENT_ID': {'type': 'string', 'required': False},
    'AZURE_SERVICE_PRINCIPAL_PASSWORD': {'type': 'string', 'required': False},
    'PRE_EXECUTION_SCRIPT': {'type': 'string', 'required': False}
}


class ServerlessDeploy(Pipe):

    def _write_gcp_keyfile(self, config, encoded_keyfile):
        with open(config, 'r') as config_file:
            config_data = yaml.safe_load(config_file.read())
            location = config_data.get('provider', None).get('credentials', None)
        if location is None:
            logger.warn('GCP key file path is not configured, can\'t write the contents of key file.')
        else:
            with open(location, 'w+') as key_file:
                key_file.write(base64.b64decode(encoded_keyfile).decode())

    def run(self):
        super().run()
        debug = self.get_variable('DEBUG')

        preexecution_script = self.get_variable('PRE_EXECUTION_SCRIPT')
        if preexecution_script and not os.path.exists(preexecution_script):
            self.fail(f'Passed pre-execution script file {preexecution_script} does not exist.')
        if preexecution_script:
            try:
                logger.info(f'Executing pre-execution script before pipe running sh {preexecution_script}')
                result = subprocess.run(['sh', preexecution_script], check=True, capture_output=True, text=True)
                if debug:
                    logger.debug(f'Executed pre-execution script. Output: {result.stdout}')
            except subprocess.CalledProcessError as exc:
                self.fail(f'{exc.output} {exc.stderr}')

        logger.info('Deploying your serverless application...')

        config = self.get_variable('CONFIG')
        extra_args = self.get_variable('EXTRA_ARGS')

        if not os.path.exists(config):
            self.fail(f'{config} doesn\'t exist.')

        gcp_key_encoded = self.get_variable('GCP_KEY_FILE')

        if gcp_key_encoded is not None:
            self._write_gcp_keyfile(config, gcp_key_encoded)

        if debug:
            os.environ['SLS_DEBUG'] = '*'
            verbose = '--verbose'
        else:
            verbose = ''

        cmd = f"serverless deploy -c {config} {extra_args} {verbose}"
        logger.debug(cmd)
        result = subprocess.run(cmd.split(), stdout=sys.stdout, stderr=sys.stderr)

        if result.returncode != 0:
            self.fail(message="Deployment failed :(")

        self.success('Deployment completed successfully!')


if __name__ == '__main__':
    pipe = ServerlessDeploy(schema=schema, check_for_newer_version=True)
    pipe.run()
