# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.1

- patch: Update README doc with custom CONFIG example.

## 1.1.0

- minor: Support preexecution hook before executing pipe.

## 1.0.0

- major: Update NodeJS to 12 LTS version.
- major: Update the Serverless Framework to 2.* version.
- minor: Add support for Python: serverless-python-requirements, serverless-wsgi.

## 0.2.2

- patch: Internal maintenance: improve test infrastructure workflow.

## 0.2.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.1.6

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.1.5

- patch: Internal maintenance: Add gitignore secrets.

## 0.1.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.1.3

- patch: Internal maintenance: Add check for newer version.

## 0.1.2

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.1

- patch: Updated readme with GCP examples.

## 0.1.0

- minor: Initial release
- patch: Internal maintenance: update pipes toolkit version

