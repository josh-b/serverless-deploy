import os
import pytest

from bitbucket_pipes_toolkit.test import PipeTestCase


class DeployAWSServerlessDashboard(PipeTestCase):

    def setUp(self):
        os.chdir('test/aws-service-dashboard')

    def tearDown(self):
        os.chdir('../..')

    @pytest.mark.skip(reason="This test requires an AdministratorAccess"
                      "permissions to be granted to an external account")
    def test_build_successfully_started_serverless_key(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'SERVERLESS_ACCESS_KEY': os.getenv('SERVERLESS_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')


class DeployAWS(PipeTestCase):

    def setUp(self):
        os.chdir('test/aws-service')

    def tearDown(self):
        os.chdir('../..')

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_successful_other_config(self):
        result = self.run_container(environment={
            'CONFIG': 'other-serverless.yml',
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_fails_without_access_key_id(self):
        result = self.run_container(environment={
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'Deployment failed')

    def test_deployment_fails_without_secret_access_key(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
        })

        self.assertRegexpMatches(
            result, r'Deployment failed')

    def test_extra_logs_present_if_debug_with_preexecution_hook(self):
        preexecution = '.preexecution-hook.sh'
        with open(preexecution, 'w') as f:
            f.write('npm install serverless-http@2.*')
        os.chmod(preexecution, 0o0005)

        result = self.run_container(environment={
            'DEBUG': 'true',
            'PRE_EXECUTION_SCRIPT': preexecution
        })

        self.assertRegexpMatches(
            result, r'Serverless: Load command config')

    def test_extra_args(self):
        result = self.run_container(environment={
            'EXTRA_ARGS': '--verbose --force',
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
        })

        self.assertRegexpMatches(
            result, r'CloudFormation - UPDATE_IN_PROGRESS')

    def test_pipe_fails_wrong_config_path(self):
        invalid_path = 'no-such-yaml.yml'
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'CONFIG': invalid_path
        })

        self.assertRegexpMatches(
            result, rf'{invalid_path} doesn\'t exist')

    def test_deploy_preexecution_hook_does_not_exist(self):
        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'PRE_EXECUTION_SCRIPT': '.does-not-exist.sh'
        })
        self.assertIn('Passed pre-execution script file .does-not-exist.sh does not exist.', result)

    def test_deploy_preexecution_hook_failed(self):
        preexecution = '.preexecution-hook.sh'
        with open(preexecution, 'w') as f:
            f.write('npm install non-existing-package@2.*')
        os.chmod(preexecution, 0o0005)

        result = self.run_container(environment={
            'AWS_ACCESS_KEY_ID': os.getenv('AWS_ACCESS_KEY_ID'),
            'AWS_SECRET_ACCESS_KEY': os.getenv('AWS_SECRET_ACCESS_KEY'),
            'PRE_EXECUTION_SCRIPT': preexecution
        })
        self.assertIn("npm ERR! 404 \nnpm ERR! 404  'non-existing-package@2.*' is not in the npm registry",
                      result)


class DeployGCP(PipeTestCase):

    def setUp(self):
        os.chdir('test/gcp-service')

    def tearDown(self):
        os.chdir('../..')
        if os.path.exists('test/gcp-service/gcp-keyfile.json'):
            os.remove('test/gcp-service/gcp-keyfile.json')

    def test_build_successfully_started_master(self):
        result = self.run_container(environment={
            'GCP_KEY_FILE': os.getenv('GCP_KEY_FILE'),
        })

        self.assertRegexpMatches(
            result, r'✔ Deployment completed successfully!')

    def test_deployment_fails_without_keyfile(self):
        result = self.run_container(environment={})

        self.assertRegexpMatches(
            result, r'Deployment failed')
